#pragma once
#include <opencv2/core.hpp>
#include <math.h>
#include <stdio.h>
#include <iomanip>
#include <armadillo>
#include <set>

using namespace std;
using namespace arma;

class Matrix_Math
{
public:
	static void Normalize(cv::Mat input, cv::Mat output);
	static void Normalize(mat input, mat& output);
	static void EpsilonNormalize(mat input, mat& output);
	static mat FromCvMat(cv::Mat input);
	static cv::Mat FromArmaMat(mat input);
	static cv::Mat FlipMatrix(cv::Mat input);
	static mat FlipMatrix(mat input);
	static vec HuMoments(const mat &input);
	static vec VarMoments(const mat &input);
	static vec NormVarMoments(const mat &input);
	static vec NormVarMoranMoments(const mat &input);
	static double Mean(mat input, mat &mask);
	static double EuclideanDistance(mat &start, mat &end);
	static double EuclideanDistanceVal(mat start, mat end);
	static void ScaleNormalize(mat input, mat& output);
	static mat ScaleNormalize(mat input);
	static vec ScaleNormalize(vec input);
	static void ScaleMatrix(mat input, mat& output, double maxInput);
	static double RandomizedCorrelationCoefficient(vec start, vec end, int samples);
	static double RandomizedCorrelationCoefficient(mat start, mat end, int samples);
	static double FuzzyCorrelationCoefficient(mat start, mat end, int samples);
	static double EnergyDistanceTB(mat start, mat end, int samples);
	static double EnergyDistanceTU(mat start, mat end, int samples);
	static double EnergyDistanceTUD(mat start, mat end, int samples); // Energy distance, weighting each comparison by distance
	static double EnergyDistance(mat start, mat end);
	static double CorrelationCoefficient(mat start, mat end);
	static mat DecimalMatrixToBinaryMatrix(mat &input);
	static mat BinaryCondition(mat &input);
	static double Distribution(mat &input);
	static double AngularSimilarity(mat &start, mat &end);
	static mat ExtractPatch(mat &input, int patch, int totalPatchCount);
	static vector<mat> ExtractPatches(mat &input, int size);
	static vector<float> ConvResizeMat(mat &input, int newRows, int newCols);
	static vector<float> ConvMat(mat &input);
	static double CosineSimilarity(mat &start, mat &end);
	static vec MatrixToMoments(mat &input);
	static vec MatrixToMoments(mat &input, int xDivisions, int yDivisions);
	static double LocallySensitiveDistance(mat &a, mat &b);
	static mat SubtractMean(mat input);
	static double AdjustedCorrelationCoefficient(mat start, mat end);
	static double AlignedAdjustedCorrelationCoefficient(mat start, mat end);
	static mat AdjustedCorrelationCoefficientZeroSpaceAbs(mat start, mat end);
	static double AdjustedCorrelationCoefficientZeroSpace(mat start, mat end);
	static double AdjustedCorrelationCoefficientZeroSpace(mat start, mat end, int n);
	static double AdjustedCorrelationCoefficientMetric(mat start, mat end);
	static mat Submat(mat &input, int x, int y, int xSize, int ySize);
	static double LocationX(const mat &input);
	static double LocationY(const mat &input);
	static std::set<double> ToSet(const mat &input);
	static mat ArbitraryAddition(const mat &inputA, const mat &inputB);
	static std::vector<double> ToVectors(const mat &input);
	static vec CarefulDivide(vec input, vec val);
	static vec CarefulDivide(vec input, double val);
	static vec SafeDivide(vec input, vec val);

	static double getSigFigs(double val, int sigFigs)
	{
		std::stringstream lStream;
		lStream << std::setprecision(sigFigs) << val;
		return std::stod(lStream.str());
	}

	// Convert the vector position to matrix position
	static pair<int, int> getCoords(int i, int cols)
	{
		double x = i / cols, y = i % cols;
		return pair<int, int>(x, y);
	}
};