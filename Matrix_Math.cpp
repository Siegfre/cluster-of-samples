#include "Matrix_Math.h"
#include <opencv2/imgproc.hpp>
#include <windows.h>
#include "Moran.h"

#define _USE_MATH_DEFINES
#include <math.h>

vector<float> Matrix_Math::ConvResizeMat(mat &input, int newRows, int newCols)
{
	mat resizedInput = mat(input);
	resizedInput.resize(newRows, newCols);
	return conv_to<vector<float>>::from(vectorise(resizedInput));
}

vector<float> Matrix_Math::ConvMat(mat &input)
{
	return conv_to<vector<float>>::from(vectorise(input));
}

vector<mat> Matrix_Math::ExtractPatches(mat &input, int size)
{
	vector<mat> patches = vector<mat>();

	int xSize = input.n_rows / size,
		ySize = input.n_cols / size;

	for (int x = 0; x < size; x++)
	{
		for (int y = 0; y < size; y++)
		{
			patches.push_back(input.submat(x * xSize, y * ySize, arma::size(xSize, ySize)));
		}
	}

	return patches;
}

mat Matrix_Math::ExtractPatch(mat &input, int patch, int totalPatchCount)
{
	const int localPatchCount = max(pow(totalPatchCount, 2) - 1, 1);
	const int SQR_PATCH_COUNT = pow(totalPatchCount, 2);
	const int rows = input.n_rows / SQR_PATCH_COUNT;
	const int cols = input.n_cols / SQR_PATCH_COUNT;
	const int rowSize = input.n_rows / totalPatchCount,
		colSize = input.n_cols / totalPatchCount;
	pair<int, int> patchPos = getCoords(patch, localPatchCount);
	
	return input(patchPos.first * rows, patchPos.second * cols, size(rowSize, colSize));
}

void Matrix_Math::Normalize(cv::Mat input, cv::Mat output)
{
	double length = cv::norm(input, cv::NormTypes::NORM_L2);
	cv::divide(input, length, output);
}

void Matrix_Math::Normalize(mat input, mat& output)
{
	output = reshape(normalise(vectorise(input), 2), input.n_rows, input.n_cols);
}

double Matrix_Math::Distribution(mat &input)
{
	double totalDistribution = 0.0;

	for (int i = 0; i < input.size() - 1; i += 2)
		totalDistribution += (input(i) - input(i + 1));

	return totalDistribution;
}

// Transform the input matrix such that all elements within the output matrix add up to 1.0
void Matrix_Math::ScaleNormalize(mat input, mat& output)
{
	double size = accu(input);

	if (size > 0)
		output = input / size;
	else
		output = input;
}

// Transform the input matrix such that all elements within the output matrix add up to 1.0
mat Matrix_Math::ScaleNormalize(mat input)
{
	mat output = mat();
	double size = accu(input);

	if (size > 0)
		output = input / size;
	else
		output = input;

	return output;
}

vec Matrix_Math::ScaleNormalize(vec input)
{
	vec output = vec();
	double size = accu(input);

	if (size > 0)
		output = input / size;
	else
		output = input;

	return output;
}

// Divide the input by the val without causing nans and infs
vec Matrix_Math::SafeDivide(vec input, vec val)
{
	vec result = vec(input);

	for (int i = 0; i < result.size(); i++)
	{
		// Ensure that divide by zeros can't occur
		if (result(i) != 0 && val(i) != 0)
			result(i) /= val(i);
	}

	return result;
}

// Divide the input by the val without causing nans and infs
vec Matrix_Math::CarefulDivide(vec input, vec val)
{
	vec result = vec(input);

	for (int i = 0; i < result.size(); i++)
	{
		if (result(i) > 0)
			result(i) /= val(i);
	}

	return result;
}

// Divide the input by the val without causing nans and infs
vec Matrix_Math::CarefulDivide(vec input, double val)
{
	vec result = vec(input);

	for (int i = 0; i < result.size(); i++)
	{
		if (result(i) > 0)
			result(i) /= val;
	}

	return result;
}

// Scale each element within the matrix by the max input, this assures that
// the value of each element is independent and not affected by other neurons
void Matrix_Math::ScaleMatrix(mat input, mat& output, double maxInput)
{
	output = input / maxInput;
}

double Matrix_Math::CosineSimilarity(mat &start, mat &end)
{
	return dot(start, end);
}

mat Matrix_Math::SubtractMean(mat input)
{
	double spatialMean = 0.0;

	for (int i = 0; i < input.size(); i++)
		spatialMean += input(i);

	spatialMean /= input.size();

	for (int i = 0; i < input.size(); i++)
		input(i) -= spatialMean;

	return input;
}

double Matrix_Math::AdjustedCorrelationCoefficient(mat start, mat end)
{
	if (start.is_zero() || end.is_zero())
		return 0.0;

	double totalSpatialMean = 0.0;
	double startTotalSpatialMean = 0.0, endTotalSpatialMean = 0.0;
	for (int i = 0; i < start.size(); i++)
	{
		totalSpatialMean += start(i) * end(i);
		startTotalSpatialMean += pow(start(i), 2);
		endTotalSpatialMean += pow(end(i), 2);
	}

	double startRoot = 0.0, endRoot = 0.0;
	startRoot = sqrt(startTotalSpatialMean);
	endRoot = sqrt(endTotalSpatialMean);

	double result = totalSpatialMean / (startRoot * endRoot);

	return result;
}

double Matrix_Math::AlignedAdjustedCorrelationCoefficient(mat start, mat end)
{
	double totalSpatialMean = 0.0;
	double startTotalSpatialMean = 0.0, endTotalSpatialMean = 0.0;
	for (int i = 0; (i < start.size()) || (i < end.size()); i++)
	{
		if (i < start.size() && i < end.size())
			totalSpatialMean += start(i) * end(i);

		if (i < start.size())
			startTotalSpatialMean += pow(start(i), 2);

		if (i < end.size())
			endTotalSpatialMean += pow(end(i), 2);
	}

	double startRoot = 0.0, endRoot = 0.0;
	startRoot = sqrt(startTotalSpatialMean);
	endRoot = sqrt(endTotalSpatialMean);

	double result = totalSpatialMean / (startRoot * endRoot);

	return result;
}

// Randomized Correlation Coefficient, we're assuming that the inputs are the same size for efficiency
// this does not however need to be the case.
double Matrix_Math::RandomizedCorrelationCoefficient(vec start, vec end, int samples)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> sampleDist(0, start.size() - 1);

	double totalSpatialMean = 0.0;
	double startTotalSpatialMean = 0.0, endTotalSpatialMean = 0.0;
	for (int j = 0; j < samples; j++)
	{
		int elementA = sampleDist(gen),
			elementB = sampleDist(gen);

		startTotalSpatialMean += start(elementA) * start(elementB);

		elementA = sampleDist(gen);
		elementB = sampleDist(gen);

		endTotalSpatialMean += end(elementA) * end(elementB);

		elementA = sampleDist(gen);
		elementB = sampleDist(gen);

		totalSpatialMean += start(elementA) * end(elementB);
	}

	double startRoot = 0.0, endRoot = 0.0;
	startRoot = sqrt(startTotalSpatialMean);
	endRoot = sqrt(endTotalSpatialMean);

	double result = totalSpatialMean / (startRoot * endRoot);

	return abs(1.0 - result);
}

double Matrix_Math::FuzzyCorrelationCoefficient(mat start, mat end, int samples)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> sampleDist(0, start.size() - 1);

	arma::vec randomSamplesA = arma::vec(samples, fill::zeros), 
		randomSamplesB = arma::vec(samples, fill::zeros);

	for (int j = 0; j < samples; j++)
	{
		int elementA = sampleDist(gen),
			elementB = sampleDist(gen);

		randomSamplesA[j] = start(elementA);
		randomSamplesB[j] = end(elementB);
	}

	double averageStart = mean(randomSamplesA), averageEnd = mean(randomSamplesB);
	double varianceStart = var(randomSamplesA), varianceEnd = var(randomSamplesB);

	double totalSpatialMean = 0.0;
	for (int i = 0; i < start.size(); i++)
	{
		totalSpatialMean += (randomSamplesA(i) - averageStart) * (randomSamplesB(i) - averageEnd) / (samples - 1);
	}

	return totalSpatialMean / (varianceStart * varianceEnd);
}

// Probabilistic version of an energy distance test.
// This version of the test is Biased towards exact spatial matches.
// The bulk of the comparison however is the similarity of the two probability distributions.
// 0 for perfect match, 1 for no match.
double Matrix_Math::EnergyDistanceTB(mat start, mat end, int samples)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> sampleWidth(0, start.n_rows - 1);
	std::uniform_int_distribution<> sampleHeight(0, start.n_cols - 1);

	double A = 0.0, B = 0.0, C = 0.0;
	double totalSize = samples * samples;

	for (int i = 0; i < samples; i++)
	{
		for (int j = 0; j < samples; j++)
		{
			double w1 = sampleWidth(gen), w2 = sampleWidth(gen),
					h1 = sampleHeight(gen), h2 = sampleHeight(gen);

			A += pow(start(w1, h1) - end(w2, h2), 2);
			B += pow(start(w1, h1) - start(w2, h2), 2);
			C += pow(end(w1, h1) - end(w2, h2), 2);
		}
	}

	A = sqrt(A);
	B = sqrt(B);
	C = sqrt(C);

	double A2 = 2.0 * A;

	return abs((A2 - B - C) / A2);
}

// Probabilistic version of an energy distance test.
// This version of the test is unbiased towards exact spatial matches.
// The bulk of the comparison however is the similarity of the two probability distributions.
// 0 for perfect match, 1 for no match.
double Matrix_Math::EnergyDistanceTU(mat start, mat end, int samples)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> sampleWidthStart(0, start.n_rows - 1);
	std::uniform_int_distribution<> sampleHeightStart(0, start.n_cols - 1);
	std::uniform_int_distribution<> sampleWidthEnd(0, end.n_rows - 1);
	std::uniform_int_distribution<> sampleHeightEnd(0, end.n_cols - 1);

	double A = 0.0, B = 0.0, C = 0.0;

	for (int i = 0; i < samples; i++)
	{
		for (int j = 0; j < samples; j++)
		{
			double w1 = sampleWidthStart(gen), w2 = sampleWidthEnd(gen),
				h1 = sampleHeightStart(gen), h2 = sampleHeightEnd(gen);

			A += pow(start(w1, h1) - end(w2, h2), 2);

			w1 = sampleWidthStart(gen), w2 = sampleWidthEnd(gen),
				h1 = sampleHeightStart(gen), h2 = sampleHeightEnd(gen);

			B += pow(start(w1, h1) - start(w2, h2), 2);

			w1 = sampleWidthStart(gen), w2 = sampleWidthEnd(gen),
				h1 = sampleHeightStart(gen), h2 = sampleHeightEnd(gen);

			C += pow(end(w1, h1) - end(w2, h2), 2);
		}
	}

	A = sqrt(A);
	B = sqrt(B);
	C = sqrt(C);

	double A2 = 2.0 * A;

	return abs((A2 - B - C) / A2);
}

// Probabilistic version of an energy distance test.
// This version of the test is unbiased towards exact spatial matches.
// The bulk of the comparison however is the similarity of the two probability distributions.
// 0 for perfect match, 1 for no match.
// We weight every element by distance, such that matches that are further away are worth less than matches that are nearby
double Matrix_Math::EnergyDistanceTUD(mat start, mat end, int samples)
{
	static std::random_device rd;
	static std::mt19937 gen(rd());
	std::uniform_int_distribution<> sampleWidthStart(0, start.n_rows - 1);
	std::uniform_int_distribution<> sampleHeightStart(0, start.n_cols - 1);
	std::uniform_int_distribution<> sampleWidthEnd(0, end.n_rows - 1);
	std::uniform_int_distribution<> sampleHeightEnd(0, end.n_cols - 1);

	double halfRows = start.n_rows, halfCols = start.n_cols;

	double A = 0.0, B = 0.0, C = 0.0;
	double adjustedDistance = 0.0;

	for (int i = 0; i < samples; i++)
	{
		for (int j = 0; j < samples; j++)
		{
			double w1 = sampleWidthStart(gen), w2 = sampleWidthEnd(gen),
				h1 = sampleHeightStart(gen), h2 = sampleHeightEnd(gen);

			adjustedDistance = 1.0 / pow(abs(w1 - w2) + abs(h1 - h2) + 1, 2.0); // Inverse square falloff
			A += pow(start(w1, h1) - end(w2, h2), 2) * adjustedDistance;
			B += pow(start(w1, h1) - start(w2, h2), 2) * adjustedDistance;
			C += pow(end(w1, h1) - end(w2, h2), 2) * adjustedDistance;
		}
	}

	A = sqrt(A);
	B = sqrt(B);
	C = sqrt(C);

	double A2 = 2.0 * A;

	return abs((A2 - B - C) / A2);
}

// Exact version of an energy distance test.
// This version of the test is unbiased towards exact spatial matches.
// The bulk of the comparison however is the similarity of the two probability distributions.
// 0 for perfect match, 1 for no match.
double Matrix_Math::EnergyDistance(mat start, mat end)
{
	double A = 0.0, B = 0.0, C = 0.0;

	for (int i = 0; i < start.size(); i++)
	{
		for (int j = 0; j < end.size(); j++)
		{
			A += pow(start(i) - end(j), 2);
			B += pow(start(i) - start(j), 2);
			C += pow(end(i) - end(j), 2);
		}
	}

	A = sqrt(A);
	B = sqrt(B);
	C = sqrt(C);

	double A2 = 2.0 * A;

	return abs((A2 - B - C) / A2);
}

// Randomized Correlation Coefficient, we're assuming that the inputs are the same size for efficiency
// this does not however need to be the case.
double Matrix_Math::RandomizedCorrelationCoefficient(mat start, mat end, int samples)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> sampleDist(0, start.size() - 1);

	double totalSpatialMean = 0.0;
	double startTotalSpatialMean = 0.0, endTotalSpatialMean = 0.0;
	for (int j = 0; j < samples; j++)
	{
		int elementA = sampleDist(gen),
			elementB = sampleDist(gen);

		startTotalSpatialMean += start(elementA) * start(elementB);
		endTotalSpatialMean += end(elementA) * end(elementB);
		totalSpatialMean += start(elementA) * end(elementB);
	}

	double startRoot = 0.0, endRoot = 0.0;
	startRoot = sqrt(startTotalSpatialMean);
	endRoot = sqrt(endTotalSpatialMean);

	double result = totalSpatialMean / (startRoot * endRoot);

	return result;
}

mat Matrix_Math::AdjustedCorrelationCoefficientZeroSpaceAbs(mat start, mat end)
{
	mat zeroStart = SubtractMean(start), zeroEnd = SubtractMean(end);

	arma::mat totalSpatialMean = mat(start.n_rows, start.n_cols, fill::zeros);
	arma::mat startTotalSpatialMean = mat(start.n_rows, start.n_cols, fill::zeros), endTotalSpatialMean = mat(start.n_rows, start.n_cols, fill::zeros);
	for (int i = 0; i < zeroStart.size(); i++)
	{
		totalSpatialMean += zeroStart(i) * zeroEnd(i);
		startTotalSpatialMean += pow(zeroStart(i), 2);
		endTotalSpatialMean += pow(zeroEnd(i), 2);
	}

	mat startRoot = mat(start.n_rows, start.n_cols, fill::zeros), endRoot = mat(start.n_rows, start.n_cols, fill::zeros);
	startRoot = sqrt(startTotalSpatialMean);
	endRoot = sqrt(endTotalSpatialMean);

	mat result = totalSpatialMean / (startRoot * endRoot);

	return result;
}

// Subtract the spatial mean prior to calculating the adjusted correlation coefficient
double Matrix_Math::AdjustedCorrelationCoefficientZeroSpace(mat start, mat end)
{
	mat zeroStart = SubtractMean(start), zeroEnd = SubtractMean(end);

	double totalSpatialMean = 0.0;
	double startTotalSpatialMean = 0.0, endTotalSpatialMean = 0.0;
	for (int i = 0; i < zeroStart.size(); i++)
	{
		totalSpatialMean += zeroStart(i) * zeroEnd(i);
		startTotalSpatialMean += pow(zeroStart(i), 2);
		endTotalSpatialMean += pow(zeroEnd(i), 2);
	}

	double startRoot = 0.0, endRoot = 0.0;
	startRoot = sqrt(startTotalSpatialMean);
	endRoot = sqrt(endTotalSpatialMean);

	double result = totalSpatialMean / (startRoot * endRoot);

	if (isnan(result))
		result = 0.0;

	return result;
}

// Subtract the spatial mean prior to calculating the adjusted correlation coefficient
double Matrix_Math::AdjustedCorrelationCoefficientZeroSpace(mat start, mat end, int n)
{
	mat zeroStart = SubtractMean(start), zeroEnd = SubtractMean(end);

	double totalSpatialMean = 0.0;
	double startTotalSpatialMean = 0.0, endTotalSpatialMean = 0.0;
	for (int i = 0; i < zeroStart.size(); i++)
	{
		totalSpatialMean += zeroStart(i) * zeroEnd(i);
		startTotalSpatialMean += pow(zeroStart(i), 2);
		endTotalSpatialMean += pow(zeroEnd(i), 2);
	}

	double startRoot = 0.0, endRoot = 0.0;
	startRoot = sqrt(startTotalSpatialMean);
	endRoot = sqrt(endTotalSpatialMean);

	double result = totalSpatialMean / (startRoot * endRoot);

	return result * (1.0 + (1.0 - pow(result, 2)) / (2.0 * n));
}

double Matrix_Math::AdjustedCorrelationCoefficientMetric(mat start, mat end)
{
	double totalSpatialMean = 0.0;
	double startTotalSpatialMean = 0.0, endTotalSpatialMean = 0.0;
	for (int i = 0; i < start.size(); i++)
	{
		totalSpatialMean += start(i) * end(i);
		startTotalSpatialMean += pow(start(i), 2);
		endTotalSpatialMean += pow(end(i), 2);
	}

	double startRoot = 0.0, endRoot = 0.0;
	startRoot = sqrt(startTotalSpatialMean);
	endRoot = sqrt(endTotalSpatialMean);

	double result = totalSpatialMean / (startRoot * endRoot);

	return sqrt(1.0 - pow(result, 2));
}

double Matrix_Math::CorrelationCoefficient(mat start, mat end)
{
	double startSpatialMean = 0.0, endSpatialMean = 0.0;

	for (int i = 0; i < start.size(); i++)
	{
		startSpatialMean += start(i);
		endSpatialMean += end(i);
	}

	startSpatialMean /= start.size();
	endSpatialMean /= end.size();

	double totalSpatialMean = 0.0;
	double startTotalSpatialMean = 0.0, endTotalSpatialMean = 0.0;
	for (int i = 0; i < start.size(); i++)
	{
		totalSpatialMean += (start(i) - startSpatialMean) * (end(i) - endSpatialMean);
		startTotalSpatialMean += pow(start(i) - startSpatialMean, 2);
		endTotalSpatialMean += pow(end(i) - endSpatialMean, 2);
	}

	double startRoot = 0.0, endRoot = 0.0;
	startRoot = sqrt(startTotalSpatialMean);
	endRoot = sqrt(endTotalSpatialMean);

	double result = totalSpatialMean / (startRoot * endRoot);

	return result;
}

mat Matrix_Math::BinaryCondition(mat &input)
{
	mat output = mat(input.n_rows, input.n_cols, fill::zeros);

	for (int i = 0; i < input.size(); i++)
	{
		if (input[i] > 0)
			output[i] = 1.0;
		else
			output[i] = 0.0;
	}

	return output;
}

double Matrix_Math::LocallySensitiveDistance(mat &a, mat &b)
{
	double globalSimilarity = norm_dot(a, b);
	double localSimilarity = 0.0;

	mat scaledA, scaledB;

	ScaleNormalize(a, scaledA);
	ScaleNormalize(b, scaledB);

	mat inputSub = (scaledA - scaledB);
	mat inputDiff = diff(inputSub);
	mat absInputDiff = abs(inputDiff);

	double isPerfect = dot(absInputDiff, absInputDiff);

	double finalVal = accu(vectorise(absInputDiff));
	double halfVal = finalVal / 2.0;
	
	localSimilarity = 1.0 - halfVal;

	return (globalSimilarity * 0.5) + (localSimilarity * 0.5);
}

mat Matrix_Math::DecimalMatrixToBinaryMatrix(mat &input)
{
	// This assumes a single channel image input: 255 = 11111111
	mat binaryMat = mat(input.n_rows * 8, input.n_cols, fill::zeros);

	int binaryPos = 0;
	for (int i = 0; i < input.size(); i++)
	{
		int result = input(i);

		if (result == 256)
			throw new exception("Element value out of bounds!");

		while (result != 0)
		{
			binaryMat(binaryPos) = result % 2;
			result = result / 2;
			binaryPos++;
		}

		binaryPos += (8 - binaryPos);
	}

	return binaryMat;
}

void Matrix_Math::EpsilonNormalize(mat input, mat& output)
{
	double inputMean = mean(vectorise(input));
	input.transform([&](double val) { return val - inputMean; });
	double inputNorm = norm(input, 2);
	input.transform([&](double val) { return val / inputNorm; });
	output = mat(input);
}

double Matrix_Math::AngularSimilarity(mat &start, mat &end)
{
	return 1.0 - (acos(Matrix_Math::CosineSimilarity(start, end)) / 180.0);
}

double Matrix_Math::EuclideanDistance(mat &start, mat &end)
{
	return sqrt(accu(pow(start - end, 2)));
}

double Matrix_Math::EuclideanDistanceVal(mat start, mat end)
{
	return sqrt(accu(pow(start - end, 2)));
}

double Matrix_Math::Mean(mat input, mat &mask)
{
	double sum = 0;
	double count = 0;
	for (int i = 0; i < input.size(); i++)
	{
		if (mask[i] > 0.0)
		{
			sum += input[i];
			count++;
		}
	}

	return sum / count;
}

cv::Mat Matrix_Math::FlipMatrix(cv::Mat input)
{
	return cv::Mat::ones(input.rows, input.cols, CV_32F) - input;
}

mat Matrix_Math::FlipMatrix(mat input)
{
	return mat(input.n_rows, input.n_cols, fill::ones) - input;
}

mat Matrix_Math::FromCvMat(cv::Mat input)
{
	cv::Mat tempMat;
	input.convertTo(tempMat, CV_64F);
	return mat(reinterpret_cast<const double*>(tempMat.data), tempMat.rows, tempMat.cols);
}

cv::Mat Matrix_Math::FromArmaMat(mat input)
{
	cv::Mat tempMat = cv::Mat(input.n_rows, input.n_cols, CV_64F, input.memptr());
	tempMat.convertTo(tempMat, CV_32F);
	return tempMat;
}

vec Matrix_Math::HuMoments(const mat &input)
{
	cv::Mat cvInput = FromArmaMat(input);

	cv::Moments moments = cv::moments(cvInput);
	vector<double> hu(7);
	cv::HuMoments(moments, hu);

	return vec(hu);
}

vec Matrix_Math::VarMoments(const mat &input)
{
	vec var = vec(6);

	var[0] = accu(input)/input.size(); // Surface Area

	mat dx = diff(input, 1, 1);
	mat dx2 = 1.0 + pow(dx, 2);
	mat sdx2 = sqrt(dx2);
	colvec csdx2 = 1.0 + sum(sdx2, 1);
	double scsdx2 = 1.0 + sum(csdx2);
	var[1] = scsdx2 / input.size(); // X Length

	mat dy = diff(input, 1, 0);
	mat dy2 = 1.0 + pow(dy, 2);
	mat sdy2 = sqrt(dy2);
	colvec csdy2 = 1.0 + sum(sdy2, 1);
	double scsdy2 = 1.0 + sum(csdy2);
	var[2] = scsdy2 / input.size(); // Y Length

	mat padx = mat(input.n_rows, input.n_cols);

	for (int i = 0; i < input.n_rows; i++)
	{
		for (int j = 0; j < input.n_cols; j++)
		{
			padx(i, j) = j;
		}
	}

	padx += 1;

	mat apadx = input * padx;
	rowvec sapadx = sum(apadx);
	double ssapadx = sum(sapadx);

	var[3] = -ssapadx / input.size(); // Location X

	mat pady = mat(input.n_rows, input.n_cols);

	for (int i = 0; i < input.n_rows; i++)
	{
		for (int j = 0; j < input.n_cols; j++)
		{
			pady(i, j) = i;
		}
	}

	pady += 1;

	mat apady = input * pady;
	rowvec sapady = sum(apady);
	double ssapady = sum(sapady);

	var[4] = ssapady / input.size(); // Location Y

	var[5] = atan(var[1] / var[2]); // Vector angle

	return var;
}

vec Matrix_Math::NormVarMoments(const mat &input)
{
	vec var = vec(6);

	var[0] = accu(input) / input.size(); // Surface Area

	mat dx = diff(input, 1, 1);
	mat dx2 = 1.0 + pow(dx, 2);
	mat sdx2 = sqrt(dx2);
	colvec csdx2 = 1.0 + sum(sdx2, 1);
	double scsdx2 = 1.0 + sum(csdx2);
	var[1] = scsdx2 / input.size(); // X Length

	mat dy = diff(input, 1, 0);
	mat dy2 = 1.0 + pow(dy, 2);
	mat sdy2 = sqrt(dy2);
	colvec csdy2 = 1.0 + sum(sdy2, 1);
	double scsdy2 = 1.0 + sum(csdy2);
	var[2] = scsdy2 / input.size(); // Y Length

	mat padx = mat(input.n_rows, input.n_cols);

	for (int i = 0; i < input.n_rows; i++)
	{
		for (int j = 0; j < input.n_cols; j++)
		{
			padx(i, j) = j;
		}
	}

	padx += 1;

	mat apadx = input * padx;
	rowvec sapadx = sum(apadx);
	double ssapadx = sum(sapadx);

	var[3] = -ssapadx / input.size(); // Location X

	mat pady = mat(input.n_rows, input.n_cols);

	for (int i = 0; i < input.n_rows; i++)
	{
		for (int j = 0; j < input.n_cols; j++)
		{
			pady(i, j) = i;
		}
	}

	pady += 1;

	mat apady = input * pady;
	rowvec sapady = sum(apady);
	double ssapady = sum(sapady);

	var[4] = ssapady / input.size(); // Location Y

	var[5] = atan(var[1] / var[2]); // Vector angle

	Matrix_Math::Normalize(var, var);

	return var;
}

mat Matrix_Math::Submat(mat &input, int x, int y, int xSize, int ySize)
{
	int startingX = x * xSize, startingY = y * ySize,
		endX = startingX + (xSize - 1), endY = startingY + (ySize - 1);

	return input.submat(startingX, startingY, endX, endY);
}

double Matrix_Math::LocationX(const mat &input)
{
	mat fullHorizontal = mat(input.n_rows, input.n_cols, fill::zeros);

	for (int i = 0; i < fullHorizontal.n_cols; i++)
		fullHorizontal.col(i) += (i + 1);

	return accu(input % fullHorizontal);
}

double Matrix_Math::LocationY(const mat &input)
{
	mat fullVertical = mat(input.n_rows, input.n_cols, fill::zeros);

	for (int i = 0; i < fullVertical.n_rows; i++)
		fullVertical.row(i) += (i + 1);

	return accu(input % fullVertical);
}

vec Matrix_Math::NormVarMoranMoments(const mat &input)
{
	vec var = vec(7);

	var[0] = accu(input) / input.size(); // Surface Area

	mat dx = diff(input, 1, 1);
	mat dx2 = 1.0 + pow(dx, 2);
	mat sdx2 = sqrt(dx2);
	colvec csdx2 = 1.0 + sum(sdx2, 1);
	double scsdx2 = 1.0 + sum(csdx2);
	var[1] = scsdx2 / input.size(); // X Length

	mat dy = diff(input, 1, 0);
	mat dy2 = 1.0 + pow(dy, 2);
	mat sdy2 = sqrt(dy2);
	colvec csdy2 = 1.0 + sum(sdy2, 1);
	double scsdy2 = 1.0 + sum(csdy2);
	var[2] = scsdy2 / input.size(); // Y Length

	mat padx = mat(input.n_rows, input.n_cols);

	for (int i = 0; i < input.n_rows; i++)
	{
		for (int j = 0; j < input.n_cols; j++)
		{
			padx(i, j) = j;
		}
	}

	padx += 1;

	mat apadx = input * padx;
	rowvec sapadx = sum(apadx);
	double ssapadx = sum(sapadx);

	var[3] = -ssapadx / input.size(); // Location X

	mat pady = mat(input.n_rows, input.n_cols);

	for (int i = 0; i < input.n_rows; i++)
	{
		for (int j = 0; j < input.n_cols; j++)
		{
			pady(i, j) = i;
		}
	}

	pady += 1;

	mat apady = input * pady;
	rowvec sapady = sum(apady);
	double ssapady = sum(sapady);

	var[4] = ssapady / input.size(); // Location Y

	var[5] = atan(var[1] / var[2]); // Vector angle

	var[6] = Moran::LISA_Global_From_Source(input);

	Matrix_Math::Normalize(var, var);

	return var;
}


vec Matrix_Math::MatrixToMoments(mat &input)
{
	arma::vec hu = HuMoments(input);
	Matrix_Math::Normalize(hu, hu);

	arma::vec var = VarMoments(input);
	Matrix_Math::Normalize(var, var);

	return join_cols(hu, var);
}

static bool momentReferenceSet = false;
vec Matrix_Math::MatrixToMoments(mat &input, int xDivisions, int yDivisions)
{
	static vector<vec> huCategories = vector<vec>(xDivisions * yDivisions);

	int xSize = input.n_rows / xDivisions;
	int ySize = input.n_cols / yDivisions;

	mat piece = mat(xSize, ySize);
	vector<vec> pieces = vector<vec>(xDivisions * yDivisions);

	for (int i = 0; i < huCategories.size(); i++)
	{
		pieces[i] = vec(6, fill::zeros);

		if (!momentReferenceSet)
		{
			huCategories[i] = vec(7, fill::randu);
			Matrix_Math::Normalize(huCategories[i], huCategories[i]);
		}
	}

	momentReferenceSet = true;

	for (int i = 0; i < xDivisions; i++)
	{
		for (int j = 0; j < yDivisions; j++)
		{
			piece = input(i * xSize, j * ySize, size(xSize, ySize));

			vec hu = HuMoments(piece);
			Matrix_Math::Normalize(hu, hu);

			int bestFit = 0;
			double bestValue = -DBL_MAX;
			double curFit = 0.0;
			for (int k = 0; k < huCategories.size(); k++)
			{
				curFit = Matrix_Math::CosineSimilarity(hu, huCategories[k]);

				if (curFit > bestValue)
				{
					bestValue = curFit;
					bestFit = k;
				}
			}

			vec var = VarMoments(piece);
			Matrix_Math::Normalize(var, var);
			pieces[bestFit] += var;
		}
	}

	vec globalMoment = join_cols(HuMoments(input), VarMoments(input));
	Matrix_Math::Normalize(globalMoment, globalMoment);
	vec result = vec(globalMoment);

	for (int i = 0; i < pieces.size(); i++)
		result = join_cols(result, pieces[i]);

	return result;
}

std::set<double> Matrix_Math::ToSet(const mat &input)
{
	std::vector<double> vectorRepresentation = arma::conv_to<std::vector<double>>::from(arma::vectorise(input));
	return std::set<double>(vectorRepresentation.begin(), vectorRepresentation.end());
}

std::vector<double> Matrix_Math::ToVectors(const mat &input)
{
	return arma::conv_to<std::vector<double>>::from(arma::vectorise(input));
}

// Allow for the addition of different sized matrices
mat Matrix_Math::ArbitraryAddition(const mat &inputA, const mat &inputB)
{
	bool aLargerRow = inputA.n_rows >= inputB.n_rows;
	bool aLargerCol = inputA.n_cols >= inputB.n_cols;

	int rows = aLargerRow ? inputA.n_rows : inputB.n_rows;
	int cols = aLargerCol ? inputA.n_cols : inputB.n_cols;

	arma::mat newMat = arma::mat(rows, cols, fill::zeros);

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			if (inputA.in_range(i, j))
				newMat(i, j) += inputA(i, j);
			
			if (inputB.in_range(i, j))
				newMat(i, j) += inputB(i, j);
		}
	}

	return newMat;
}