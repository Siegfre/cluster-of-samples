#pragma once

#include <vector>
#include <armadillo>
#include "hnswlib.h"
#include "Matrix_Math.h"

#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;
using namespace arma;

using dist_t = float;
using Distance = hnswlib::InnerProductSpace;
using IndexType = std::unique_ptr<hnswlib::HierarchicalNSW<dist_t>>;

class ClusterOfSamples
{
private:
	vector<IndexType> clusterIndexes = vector<IndexType>();
	/*	The mean of an unknown number of clusters, if this grows very large we may want to just use HNSW for this.
		Though if HNSW is used, then we'll need to create a new distance formula for it.
		In the first stage we're assigning members based on ddCRP (Distance Dependent Chinese Restaurant Process),
		however when retrieving a class we're using a traditional distance function. (euclidean distance, cosine similarity, correlation coefficient, etc..)
	*/
	vector<arma::mat> clusterCentroids = vector<arma::mat>();
	vector<arma::mat> clusterStdDev = vector<arma::mat>();
	// The number of samples in the cluster, equivalent to age or n.
	vector<int> clusterSamples = vector<int>();
	/*	Represents the probability distribution for a given cluster. A sample that is similar to the cluster
	will represent each output according to its probability. We may be able to utilze Top-Knn to improve
	the models performance. */
	vector<arma::vec> clusterOutputProbabilitiesAbs = vector<arma::vec>();
	vector<arma::vec> clusterOutputProbabilities = vector<arma::vec>();
	arma::mat globalMean; // Used to help determine the global sigma value
	int SCALING_FACTOR = 200;
	/*	A value in the range [0.0, 1.0] Should be determined by least squares or some other method, perhaps this can be optimized during "sleep"
		0.2711 is a sample value taken from a paper on Neutron spectrometry, and is not optimal for general cases. */
	const double SMOOTHING_FACTOR = 0.2711;
	const int TOP_K = 3;
	int totalSamples = 0;
	double dynamicSigma = 1.0;
	int dim;
	int init_num_elements;
	int M = 48;
	const int ef_construction = 500;
	std::unique_ptr<Distance> space;
	int maxClusters = 1;
	int outputDim = 10;
	int xSize = 28, ySize = 28;
	int threshhold = 73;

public:
	ClusterOfSamples() = default;
	ClusterOfSamples(int inputDim, int _maxClusters)
	{
		dim = inputDim;
		maxClusters = _maxClusters;

		// create the index
		space = std::unique_ptr<Distance>(new Distance(dim));
		globalMean = arma::mat(28, 28, fill::randu);
	}
	ClusterOfSamples(int inputDim, int _maxClusters, int _outputDim)
	{
		dim = inputDim;
		maxClusters = _maxClusters;

		// create the index
		space = std::unique_ptr<Distance>(new Distance(dim));
		outputDim = _outputDim;
		xSize = ySize = sqrt(inputDim);
		globalMean = arma::mat(xSize, ySize, fill::randu);
		SCALING_FACTOR = maxClusters;
	}
	ClusterOfSamples(int inputDim, int _maxClusters, int _outputDim, int _xSize, int _ySize, int _threshhold)
	{
		dim = inputDim;
		maxClusters = _maxClusters;

		// create the index
		space = std::unique_ptr<Distance>(new Distance(dim));
		outputDim = _outputDim;
		xSize = _xSize;
		ySize = _ySize;
		globalMean = arma::mat(xSize, ySize, fill::randu);
		SCALING_FACTOR = maxClusters;
		threshhold = _threshhold;
	}

	int Predict(arma::mat input)
	{
		// The GRNN additions are based largely on https://www.github.com/CorcovadoMing/GeneralRegressionNeuralNetwork
		const double SIGMA_SQR = pow(dynamicSigma, 2);
		arma::vec factors = arma::vec(outputDim, fill::zeros);
		double totalAdjustedDistance = 0.0;
		for (int j = 0; j < clusterCentroids.size(); j++)
		{
			double catDistance = 1.0 - Matrix_Math::AdjustedCorrelationCoefficientZeroSpace(input, clusterCentroids[j]);
			/*	Utilize Gaussian RBF as it "It is suitable not only in generalizing a global mapping but also in refining local features" and
				"monotonically decreases with distance from the center, and form the classic bell shaped curve which maps high values into low
				ones, and maps mid-range values into high ones." */
			double adjustedDistance = exp(-catDistance / SIGMA_SQR);
			factors += clusterOutputProbabilities[j] * adjustedDistance;
			totalAdjustedDistance += adjustedDistance;
		}
		arma::vec finalResult = Matrix_Math::CarefulDivide(factors, totalAdjustedDistance);

		return finalResult.index_max();
	}

	void Learn(arma::mat input, arma::vec output)
	{
		arma::vec randVec = arma::normalise(arma::vectorise(input));
		vector<float> vectorRep = arma::conv_to<vector<float>>::from(randVec);
		float *rawRep = vectorRep.data();

		/*	Based on the distance from the input to the KNN in the sample
			determine the probability that the sample will join this cluster.
			We may extend this with top-k to get the best possible results. */
		vector<int> sampleProbabilty = vector<int>();
		for (int j = 0; j < clusterCentroids.size(); j++)
		{
			auto results = clusterIndexes[j]->searchKnn(rawRep, TOP_K);
			int totalDistance = 0;

			for (auto result = results.top(); !results.empty(); results.pop())
			{
				double topStrength = 1.0 - result.first;

				if (isnan(topStrength))
					topStrength = DBL_EPSILON;

				if (isinf(topStrength))
				{
					if (topStrength > 0)
						topStrength = 1.0;
					else
						topStrength = DBL_EPSILON;
				}

				int distance = 100 * topStrength;
				totalDistance += distance > threshhold ? distance : 0;
			}

			if (totalDistance > 0)
				totalDistance /= TOP_K;
				
			totalDistance = max(totalDistance, 1); // A minimum distance of 1 should be used

			sampleProbabilty.push_back(totalDistance);
		}

		if (clusterCentroids.size() < maxClusters)
			sampleProbabilty.push_back(SCALING_FACTOR); // The probability of sitting at a new table, 5% has been chosen for this example
		/*	The likelihood that a sample will join a cluster is proportional to the number of samples in the cluster
			The likelihood that a sample will join a new cluster is based on a constant scaling factor. */
		std::discrete_distribution<> seatingDistribution(sampleProbabilty.begin(), sampleProbabilty.end());
		std::random_device rd;
		std::mt19937 gen(rd());
		int randCluster = seatingDistribution(gen);

		if (clusterCentroids.size() == 0 || randCluster == clusterCentroids.size())
		{
			clusterCentroids.push_back(input);
			clusterStdDev.push_back(mat(xSize, ySize, fill::ones));
			clusterSamples.push_back(1);
			clusterOutputProbabilitiesAbs.push_back(output);
			clusterOutputProbabilities.push_back(output);
			clusterIndexes.push_back(std::unique_ptr<hnswlib::HierarchicalNSW<dist_t>>(new hnswlib::HierarchicalNSW<dist_t>(space.get(), outputDim, M, ef_construction)));
			clusterIndexes.back()->addPoint(rawRep, 0);
		}
		else
		{
			// Add a new sample to the cluster index, used to determine distance
			clusterIndexes[randCluster]->addPoint(rawRep, clusterSamples[randCluster]);
			// Keep the index as small as possible to start, increase the size in chunks as necessary.
			// Elements cannot be deleted or updated, so the index will need to be resized as the network runs
			if (clusterIndexes[randCluster]->cur_element_count >= clusterIndexes[randCluster]->max_elements_)
				clusterIndexes[randCluster]->resizeIndex(clusterIndexes[randCluster]->cur_element_count * 2);

			arma::mat matDiff = clusterCentroids[randCluster] - input;

			// Update the mean for this class
			clusterSamples[randCluster]++;
			double w2 = 1.0 / clusterSamples[randCluster]; // Learning rate
			double w1 = 1.0 - w2; // Retention rate
			clusterCentroids[randCluster] = (clusterCentroids[randCluster] * w1) + (input * w2);
			clusterStdDev[randCluster] = (clusterStdDev[randCluster] * w1) + (abs(matDiff) * w2);
			clusterOutputProbabilitiesAbs[randCluster] += output;
			clusterOutputProbabilities[randCluster] = Matrix_Math::CarefulDivide(clusterOutputProbabilitiesAbs[randCluster], clusterSamples[randCluster]);
		}

		/*	Estimate the global sigma value using the average distance of each sample from the global mean.
			"Evolving General Regression Neural Networks using Limited Incremental Evolution for Data-Driven Modeling of Non-linear Dynamic Systems"
		*/
		totalSamples++;
		double gW2 = 1.0 / totalSamples; // Learning rate
		double gW1 = 1.0 - gW2; // Retention rate
		dynamicSigma = (dynamicSigma * gW1) + (pow(Matrix_Math::AdjustedCorrelationCoefficientZeroSpace(input, globalMean), 2) * gW2);
		globalMean = (globalMean * gW1) + (input * gW2);
	}
};