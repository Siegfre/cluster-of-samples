# README #

A lightweight implementation of the research paper ["Human representation of multimodal distributions as clusters of samples by  Jingwei Sun, Jian Li, and Hang Zhang"](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007047)

This paper attempts to push forward the understanding of human perception from a statistical perspective. Proposing the CoS (Cluster of Samples) model as an example of an efficient explanation of empirical observations related to human perception. The researchers perform various perception tests on participants and determine the accuracy/efficiency trade-offs made in human perception. Specifically in how human perception models the input probability distributions. A number of plausible models are proposed that would lead to the observed outcomes. The researcher compare these models, along with others in terms of their ability to explain the data.

Temporal associations, the model uses the [ddCRP (distance dependent Chinese restaurant process)](http://www.cs.columbia.edu/~blei/papers/BleiFrazier2011.pdf) to cluster samples. It should be possible to include temporal distance as a factor in the clustering, whether that be explicitly (every sample is given a timestamp) or implicitly (model activity/sample vector represents a moment in time) to create temporal associations.

Here you'll find my basic attempt at reproducing the described model. While this produces decent results, it is missing (intentional) lateral inhibition, as its not clear if the paper is implying that lateral inhibition is the result of the ddCRP clustering or if there was some additional lateral inhibition used. Additionally the paper mentions the data undergoing a power transformation, however results did not improve after implementing power transformations.

The model uses the following to approximate an efficient implementation of CoS: ddCRP for clustering, [HNSW](https://arxiv.org/abs/1603.09320) for nearest neighbor search, and [GRNN](https://en.wikipedia.org/wiki/General_regression_neural_network) with a dynamic sigma based on the sample variance for regression.

### How do I get set up? ###

* Clone the repo
* Download dependencies [Armadillo](http://arma.sourceforge.net), and [Hnswlib](https://github.com/nmslib/hnswlib)
* Use the "Learn()" and "Predict()" functions with your desired dataset for online learning based on human perception!